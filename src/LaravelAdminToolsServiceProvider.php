<?php

namespace Ekoukltd\LaravelAdminTools;

use Ekoukltd\LaravelAdminTools\View\Components\Block;
use Ekoukltd\LaravelAdminTools\View\Components\ModalContent;
use Ekoukltd\LaravelAdminTools\View\Components\Alert;
use Ekoukltd\LaravelAdminTools\View\Components\PageTitle;
use Illuminate\Support\ServiceProvider;

class LaravelAdminToolsServiceProvider extends ServiceProvider
{
    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot(): void
    {
        // $this->loadTranslationsFrom(__DIR__.'/../resources/lang', 'ekoukltd');
        $this->loadViewsFrom(__DIR__.'/../resources/views', 'lat');
        // $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
        $this->loadRoutesFrom(__DIR__.'/../routes/admin.php');
        $this->loadViewComponentsAs('admintools',[
            Block::class,
            ModalContent::class,
            Alert::class,
            PageTitle::class
            
        ]);
		
		
        // Publishing is only necessary when using the CLI.
        if ($this->app->runningInConsole()) {
            $this->bootForConsole();
        }
    }

    /**
     * Register any package services.
     *
     * @return void
     */
    public function register(): void
    {
        $this->mergeConfigFrom(__DIR__.'/../config/laravel-admin-tools.php', 'laravel-admin-tools');

        // Register the service the package provides.
        $this->app->singleton('laravel-admin-tools', function ($app) {
            return new LaravelAdminTools;
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['laravel-admin-tools'];
    }

    /**
     * Console-specific booting.
     *
     * @return void
     */
    protected function bootForConsole(): void
    {
        // Publishing the configuration file.
        $this->publishes([
            __DIR__.'/../config/laravel-admin-tools.php' => config_path('laravel-admin-tools.php'),
        ], 'laravel-admin-tools.config');

        // Publishing the views.
        $this->publishes([
            __DIR__.'/../resources/views' => base_path('resources/views/vendor/ekoukltd/laravel-admin-tools'),
        ], 'laravel-admin-tools.views');

        // Publishing assets.
        $this->publishes([
            __DIR__.'/../resources/assets' => base_path('resources/assets/vendor/ekoukltd/laravel-admin-tools'),
        ], 'laravel-admin-tools.views');
	
	    // Publishing Datatables.
	    $this->publishes([
		                     __DIR__.'/../resources/datatables/views' => base_path('resources/views/vendor/datatables'),
	                     ], 'laravel-admin-tools.views');

        // Publishing the translation files.
        /*$this->publishes([
            __DIR__.'/../resources/lang' => resource_path('lang/vendor/ekoukltd'),
        ], 'laravel-admin-tools.views');*/

        // Registering package commands.
        // $this->commands([]);
    }
}
