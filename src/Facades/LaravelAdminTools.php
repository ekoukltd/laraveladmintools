<?php

namespace Ekoukltd\LaravelAdminTools\Facades;

use Illuminate\Support\Facades\Facade;

class LaravelAdminTools extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor(): string
    {
        return 'laravel-admin-tools';
    }
}
