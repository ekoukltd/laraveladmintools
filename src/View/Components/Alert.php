<?php

namespace Ekoukltd\LaravelAdminTools\View\Components;

use Illuminate\View\Component;

class Alert extends Component
{
    /**
     * The alert type.
     *
     * @var string
     */
    public $type;
    
    /**
     * The alert message.
     *
     * @var string
     */
    public $message;
    
    public $dismiss;
    
    /**
     * Create the component instance.
     *
     * @param  string  $type
     * @param  string  $message
     * @return void
     */
    public function __construct($message, $dismiss = true, $type = null)
    {
        $this->type = $type ?? 'info';
        $this->message = $message;
        $this->dismiss = $dismiss;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('lat::components.alert');
    }
}
