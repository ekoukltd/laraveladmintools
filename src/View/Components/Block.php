<?php

namespace Ekoukltd\LaravelAdminTools\View\Components;

use Illuminate\View\Component;
use \Illuminate\Support\Str;
class Block extends Component
{
    /**
     * The alert type.
     *
     * @var string
     */
    public $title;
    public $buttons;
    public $headerclass;
    public $id;
    public $blockclass;
    public $subhead;
    public $dataset;
    
    /**
     * Create the component instance.
     *
     * @param  string  $title
     * @param  string  $buttons
     * @return void
     */
    public function __construct($title, $buttons = null, $headerclass=null,$id=null, $blockclass=null,$subhead = null,$dataset = null
    )
    {
        $this->title = $title;
        $this->buttons = $buttons;
        $this->headerclass = $headerclass;
        $this->blockclass = $blockclass;
        $this->subhead = $subhead;
        $this->id = $id??(Str::camel(preg_replace('/[^a-zA-z]/','',strip_tags($title))??'block_'.Str::random(6)));
        $this->dataset = $dataset;
    }
    
    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('lat::components.block');
    }
}
