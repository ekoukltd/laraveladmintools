<?php

namespace Ekoukltd\LaravelAdminTools\View\Components;

use Illuminate\View\Component;

class ModalContent extends Component
{
    /**
     * The alert type.
     *
     * @var string
     */
    public $title;
    
    public $buttons;
    
    public $footerButtons;
    
    public $headerColour;
    
    /**
     * Create the component instance.
     *
     * @param  string  $title
     * @param  string  $buttons
     * @return void
     */
    public function __construct($title, $buttons=null,$footerButtons=null,$headerColour=null)
    {
        $this->title = $title;
        $this->buttons = $buttons;
        $this->footerButtons = $footerButtons;
        $this->headerColour = $headerColour;
    }
    
    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('lat::components.modal-content');
    }
}
