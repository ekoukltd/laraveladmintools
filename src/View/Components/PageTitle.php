<?php

namespace Ekoukltd\LaravelAdminTools\View\Components;

use Illuminate\View\Component;

class PageTitle extends Component
{

    public $icon;
    
    public $title;
    
    public $subtitle;
    

    public function __construct($title,$subtitle = null,$icon = null)
    {
        $this->icon = $icon;
        $this->title = $title;
        $this->subtitle = $subtitle;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('lat::components.page_header_h1');
    }
}
