// Helpers
export default class Helpers {

    /**
     * Provide a method to run specific helpers
     * @param helpers
     * @param options
     */
   static run(helpers, options = {}) {
       let allHelpers = {
           initCSRF: ()=> this.initCSRF(),
           initAdmin: () => this.initAdmin(),
           initHotKeys: () => this.initHotKeys(),
           notify: (options)=> this.notify(options),
           select2: () => this.select2(),
           froala: () => this.froala(),
           toggleAjax: () => this.toggleAjax(),
           initDatatablesFilters: () => this.initDatatablesFilters(),
           initAjaxLoadModal: () => this.initAjaxLoadModal(),
           ajaxLoadModal: () => this.ajaxLoadModal(),
           initAjaxForms: () => this.initAjaxForms()

       };

       if (helpers instanceof Array) {
           for (let index in helpers) {
               if (allHelpers[helpers[index]]) {
                   allHelpers[helpers[index]](options);
               }
           }
       } else {
           if (allHelpers[helpers]) {
               allHelpers[helpers](options);
           }
       }
   }

    /**
     * Add Laravel CSRF token from meta tag
     */
   static initCSRF()
   {
       jQuery.ajaxSetup({
           headers: {
               'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
           }
       });
   }
    /*
     * Select2, for more examples you can check out https://github.com/select2/select2
     * The only option for select inputs
     */
    static select2() {
        jQuery('.js-select2:not(.js-select2-enabled)').each((index, element) => {
            let el = jQuery(element);
            el.addClass('js-select2-enabled').select2({
                placeholder: el.data('placeholder') || false
            });
        });
    }

    /**
     * Bootstrap Growl  - pretty popup notifications
     * This for bootstrap 5
     * @param options
     */
    static notify(options = {}) {
        // Create notification
        jQuery.notify({
                icon: options.icon || '',
                message: options.message,
                url: options.url || ''
            },
            {
                element: options.element || 'body',
                type: options.type || 'info',
                placement: {
                    from: options.from || 'top',
                    align: options.align || 'right'
                },
                allow_dismiss: (options.allow_dismiss === false) ? false : true,
                newest_on_top: (options.newest_on_top === false) ? false : true,
                showProgressbar: options.show_progress_bar ? true : false,
                offset: options.offset || 20,
                spacing: options.spacing || 10,
                z_index: options.z_index || 1033,
                delay: options.delay || 5000,
                timer: options.timer || 100000,
                animate: {
                    enter: options.animate_enter || 'animated fadeIn',
                    exit: options.animate_exit || 'animated fadeOutDown'
                },
                template: `<div data-notify="container" class="col-11 col-sm-4 alert alert-{0} alert-dismissible" role="alert">
  <p class="mb-0">
    <span data-notify="icon"></span>
    <span data-notify="title">{1}</span>
    <span data-notify="message">{2}</span>
  </p>
  <div class="progress" data-notify="progressbar">
    <div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>
  </div>
  <a href="{3}" target="{4}" data-notify="url"></a>
  <a class="p-2 m-1 text-dark" href="javascript:void(0)" aria-label="Close" data-notify="dismiss">
    <i class="fa fa-times"></i>
  </a>
</div>`
            });
    }
    static initHotKeys()
    {
        tinykeys(window, {
            "Shift+D": () => {
                alert("The 'Shift' and 'd' keys were pressed at the same time")
            },
            "y e e t": () => {
                alert("The keys 'y', 'e', 'e', and 't' were pressed in order")
            },
            "$mod+KeyD": event => {
                event.preventDefault()
                this.initAdmin()
            },
        })
    }

    static initAdmin()
    {
        if ($('#laravel-admin-tools').length === 0) {
            var iframe = document.createElement('iframe');
            iframe.onload = function() {
                iframe.classList.add("is-open");
            };
            //Todo get this URL from the page so it's not hardcoded
            iframe.src = 'http://laravel/admin-tools';
            iframe.id = 'laravel-admin-tools';
            document.body.appendChild(iframe);
        } else {
            //Element exists, just toggle show and hide
            $('#laravel-admin-tools').toggleClass('is-open');
        }
    }


    static initDatatablesFilters()
    {
        //Use a dropdown to update datatable
        jQuery('.dt-filter-select select').on('select2:select', function (e) {
            var data = e.params.data;
            let param = jQuery(this).closest('.dt-filter-select').data('filter');
            let url = new URL(window.location.href);
            url.searchParams.set(param, data.id);
            window.location.href = url.href;
        });
    }


    /**
     * Load modal content via ajax
     * Requires an empty modal with ID of emptyModal
     * Trigger button requires data-href=URL to load data from
     */
    static initAjaxLoadModal() {

        jQuery(document).on('click', '.ajaxSave', function () {
            jQuery(this).attr('disabled', true);
            jQuery('.loading').show();
            jQuery(this).closest('form').trigger('submit');
        });

        jQuery('#emptyModal').on('show.bs.modal', function (event) {
            var button = jQuery(event.relatedTarget);

            if (button.data('size') !== undefined) {
                jQuery(this).find('.modal-dialog').addClass(button.data('size'));
            }
            let options = {};

            if (button.data('options') !== undefined) {
                options = button.data('options');
            }

            if (button.data('picked_date') !== undefined) {
                options.picked_date = button.data('picked_date');
            }

            window.LaraAdminTools.loadAjaxModal(button.data('href'), 'modal_content', options);
        });


        jQuery('#emptyModal').on('hidden.bs.modal', function () {
            //remove content on close and replace with content spinner
            jQuery("#modal_content").html('<div class="block-content block-content-full text-center border-top">\n' +
                '                <i class="fa fa-4x fa-cog fa-spin text-warning my-8"></i>\n' +
                '            </div>');

            jQuery(this).find('.modal-dialog').removeClass('modal-lg modal-xl modal-sm');
        });

    }


    static ajaxLoadModal(sourceUrl, target, options) {

        target = typeof target !== 'undefined' ? target : 'content';
        var paramData = typeof options !== 'undefined' ? options : {};
        jQuery.ajax({
            type: "GET",
            url: sourceUrl,
            contentType: false,
            data: {options: paramData},
            success: function (data) {
                if('success' in data && 'html' in data){
                    jQuery("#" + target).html(data.html);
                }

            },
            error: function (xhr, status, error) {
                console.log(xhr.responseText);
            }
        });
    }

    //Common Ajax Form Submission
    static ajaxFormSubmit(form, customSuccess) {

        jQuery.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content'),
            }
        });

        var data = new FormData();

        //Form data
        var form_data = jQuery(form).serializeArray();
        jQuery.each(form_data, function (key, input) {
            data.append(input.name, input.value);
        });

        jQuery.ajax({
            url: form.attr('action'),
            method: 'POST',
            data: data,
            cache: false,
            processData: false,
            contentType: false,
            success: function (response) {

                if (response.success) {
                    if (!response.hasOwnProperty('dont_notify')) {
                        window.LaraAdminTools.helpers('notify', {type: 'success', icon: 'fa fa-check mr-1', message: response.message});
                    }
                    if (typeof customSuccess !== 'undefined') {
                        customSuccess(response);
                    }
                } else if (response.error) {
                    jQuery.each(response.errors, function (field_name, error) {
                        window.LaraAdminTools.helpers('notify', {type: 'danger', icon: 'fa fa-times mr-1', message: field_name + " : " + error});
                        jQuery(document).find('.invalid-feedback').remove();
                        let el = jQuery(document).find('[name=' + field_name + ']');
                        el.addClass('form-control-alt is-invalid');
                        el.after('<p class="invalid-feedback animated fadeIn my-0"><i class="fa fa-warning"></i>' + error + '</p>');

                    });

                }

            },
            error: function (response) {
                if (response.status === 422) {
                    jQuery(document).find('.invalid-feedback').remove();
                    jQuery.each(response.responseJSON.errors, function (field_name, error) {
                        window.LaraAdminTools.helpers('notify', {type: 'danger', icon: 'fa fa-times mr-1', message: field_name + " : " + error});
                        let el = jQuery(document).find('[name=' + field_name + ']');
                        el.addClass('form-control-alt is-invalid');
                        el.after('<p class="invalid-feedback animated fadeIn my-0"><i class="fa fa-warning"></i>' + error + '</p>')
                    });
                } else {
                    window.LaraAdminTools.helpers('notify', {type: 'danger', icon: 'fa fa-times mr-1', message: JSON.stringify(response.responseText)});
                }

            },
            complete: function (response) {
                form.find(':submit').attr('disabled', false);
            }
        });

    }

    static initAjaxForms() {
        //Ajax Save Form
        jQuery(document).on("submit", ".frmSaveAjax", function (e) {
            e.preventDefault();
            let btn = jQuery(this).find(':submit');
            btn.attr("disabled", true);
            window.LaraAdminTools.ajaxFormSubmit(jQuery(this), function (data) {
                location.reload();
            });
        });
    }

    static toggleAjax()
    {
        jQuery('.js-toggle-ajax:not(.js-toggle-ajax-enabled)').each((index, element) => {
            jQuery(element).addClass('js-toggle-ajax-enabled');
            jQuery(element).on('change', function (e) {
                let form = jQuery(element).closest('form');
                window.LaraAdminTools.ajaxFormSubmit(form);
            });
        })
    }

    /**
     * FROALA Editor - requires a licence
     * Add Key to .env under MIX_FROALA_KEY=xxxxxx
     * Enable in config file
     * Helpers.run('froala');
     * <div class="js-froala">{{ $model->text }}</div>
     */
    static froala() {
        var key=process.env.MIX_FROALA_KEY;
        jQuery('.js-froala:not(.js-froala-enabled)').each((index, element) => {
            let id=jQuery(element).attr('id');
            jQuery(element).addClass('js-froala-enabled');

            new FroalaEditor('textarea#'+id,{
                key: key,
                initOnClick: true,
                wordPasteModal: false,
                wordPasteKeepFormatting: false,
                toolbarButtons: {
                    moreText: {
                        buttons: ['bold', 'italic', 'underline', 'fontSize', 'textColor', 'backgroundColor','clearFormatting'],
                        align: 'left',
                        buttonsVisible: 7
                    },
                    moreParagraph: {
                        buttons: ['alignLeft', 'alignCenter', 'alignRight', 'alignJustify','formatUL','formatOL', 'paragraphFormat', 'paragraphStyle', 'outdent', 'indent'],
                        align: 'left',
                        buttonsVisible: 10
                    },
                    moreRich: {
                        buttons: ['insertLink','insertHR','html'],
                        align: 'left',
                        buttonsVisible: 5
                    },
                },
            });
        });
    }


}
