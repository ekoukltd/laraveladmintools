/*
 *  Document   : app.js
 *  Author     : pixelcave
 *  Description: Main entry point
 *
 */

// Import required modules
import Template from './modules/template';
import LaraAdminTools from "../lara-admin-tools";
import UserNotificationPrefs from "../../../../../user-notification-prefs/resources/assets/js/user-notification-prefs";
import Laraconsent from "../../../../../laraconsent/resources/assets/js/laraconsent";

// App extends Template
export default class App extends Template {
  /*
   * Auto called when creating a new instance
   *
   */
  constructor() {
    super();
  }

  ekoConsoleBanner() {
    var ekoContact=
        "#  Laravel Admin Tools by: https://www.ekouk.com                \n";

    var ekoLogo=
        "#                                                                              \n"+
        "#   __        __        __     ___  ___  __             __        __   __      \n"+
        "#  /  ` |    /  \\ |  | |  \\     |  |__  /  ` |__| |\\ | /  \\ |    /  \\ / _` \\ / \n"+
        "#  \\__, |___ \\__/ \\__/ |__/     |  |___ \\__, |  | | \\| \\__/ |___ \\__/ \\__>  |  \n"+
        "#                                                                              \n"+
        "#        __   __         ___  ___  __  ___  __                                 \n"+
        "#   /\\  |__) /  ` |__| |  |  |__  /  `  |  /__`                                \n"+
        "#  /~~\\ |  \\ \\__, |  | |  |  |___ \\__,  |  .__/                                \n"+
        "#                                                                              \n";
      console.log("%c"+ekoContact+"%c"+ekoLogo, 'background: #222; color: #bada55; font-size:14px','background: #222; color: #bada55;');
  }
}

// Create a new instance of App
window.One = new App();
window.One.ekoConsoleBanner();

