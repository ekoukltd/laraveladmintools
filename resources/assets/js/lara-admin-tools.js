import Imports from './imports'
import Helpers from './helpers';

export default class LaraAdminTools {

    constructor() {
        this._laraAdminToolsInit();
    }

    _laraAdminToolsInit() {
        this.helpers([
            'initCSRF',
            'initHotKeys',
        ]);
        console.log('Laravel Admin Tools Initialised');
    }

    init() {
        this._laraAdminToolsInit();
    }

    /*
     * jQuery Class Helpers
     *
     */
    helpers(helpers, options = {}) {
        Helpers.run(helpers, options);
    }

    loadAjaxModal(filename, target,options){
        Helpers.ajaxLoadModal(filename, target,options);
    }

    ajaxFormSubmit(form, customSuccess){
        Helpers.ajaxFormSubmit(form, customSuccess);
    }

}

// Once everything is loaded
jQuery(() => {
    // Create a new instance of LaraAdminTools
    window.LaraAdminTools = new LaraAdminTools();

});
