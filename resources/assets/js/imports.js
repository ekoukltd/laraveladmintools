// import 'bootstrap';
//
import './plugins/bootstrap-notify.min'
import flatpickr from "flatpickr";
import tinykeys from "tinykeys"
import 'select2'

/** Datatables Dependencies **/
try {
    window.$ = window.jQuery = require('jquery');
    require( 'datatables.net-bs5' );
    require( 'datatables.net-scroller-bs5' );
    require( 'datatables.net-responsive-bs5' );
    window.tinykeys     = tinykeys;

} catch (e) {}
