<div class="block block-themed block-transparent mb-0">
    <div class="block-header flex-sm-row {{$headerColour??'bg-primary-dark'}}">
        <h3 class="block-title">{!! $title??'' !!}</h3>
        <div class="block-options">
            {{{$buttons??''}}}
            <button type="button" class="btn-block-option" data-bs-dismiss="modal" aria-label="Close">
                <i class="fa fa-fw fa-times"></i>
            </button>
        </div>
    </div>
</div>

<div class="modal-body block-content">
    {{ $slot }}
</div>

<div class="block-content block-content-full text-end border-top">
    {{{$footerButtons??''}}}
</div>
