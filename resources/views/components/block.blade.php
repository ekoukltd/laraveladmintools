<div class="block block-rounded {{$blockclass??''}}" id="{{$id}}"
@isset($dataset)
    data-dataset="{{$dataset}}"
@endisset
>
       <div class="block-header flex-sm-row {{$headerclass??'block-header-default'}}">
            <h3 class="block-title">{!! $title??'' !!}</h3>
           @isset($subhead)
               {{{$subhead}}}
           @endisset
               @isset($buttons)
                <div class="block-options">
                    {{{$buttons}}}
                </div>
               @endisset
        </div>
    <div class="block-content">
        <div class="pb-2">
            {{ $slot }}
        </div>
    </div>
</div>