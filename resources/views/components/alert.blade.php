<div {{ $attributes->merge(['class' => 'd-flex align-items-center alert alert-'.$type]) }} role="alert">
    @switch($type)
        @case('success')
            @php($icon="check-circle")
            @break
        @case('danger')
            @php($icon="exclamation-triangle")
            @break
        @case('warning')
            @php($icon="exclamation-circle")
            @break
        @case('info')
            @php($icon="info-circle")
            @break
        @default
        @php($icon="info-circle")
    @endswitch


    <div class="flex-00-auto">
        <i class="fa fa-2x fa-fw fa-{{$icon}} mr-2 me-2"></i>
    </div>
    <div class="ml-3">
        <p class="mb-0"> {!! $message !!}</p>
    </div>

        @if(isset($dismiss) && $dismiss=="false")
        @else
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
        @endisset
</div>