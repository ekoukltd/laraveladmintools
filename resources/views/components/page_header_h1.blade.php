@php($colour = config('laravel-admin-tools.css_format')=='bootstrap5'?'text-primary':'text-white')

<h1 class="h3 fw-bold mb-2 {{$colour}}">
    @if($icon)
        <i class="fa fa-{{$icon??''}} me-2"></i>
    @endif
        {{$title??''}}
</h1>
@if($subtitle)
        <h2 class="fs-base lh-base fw-medium text-muted mb-0">
                {{$subtitle}}
        </h2>
@endif