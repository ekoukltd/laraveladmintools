<div class="modal fade print_hide" id="emptyModal" tabindex="-1" aria-labelledby="emptyModal" style="display: none;"
     aria-hidden="true">
    <div class="modal-dialog  modal-dialog-centered modal-dialog-popout modal-dialog-scrollable" role="document">
        <div id="modal_content" class="modal-content">
            <div class="block-content block-content-full text-center border-top">
                <i class="fa fa-4x fa-cog fa-spin text-warning my-8"></i>
            </div>
        </div>
    </div>
</div>
