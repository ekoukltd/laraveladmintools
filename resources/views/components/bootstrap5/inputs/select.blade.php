<div class="{{$wrapperClass??'mb-4'}}">
    @isset($label)
        <label class="form-label d-flex align-items-center" for="{{$name??''}}">{{ $label }}
            @isset($btnAfter)
                <button type="button" class="btn {{$btnAfterClass ?? ' ms-auto btn-info'}}">{{$btnAfter}}</button>
            @endisset
        </label>
    @endisset

    <select name="{{ $name ?? '' }}" id="{{ $id ?? '' }}" class="form-select js-select2 {{ $class ?? '' }}" style="{{ $style ?? 'width:100%' }}"
            @isset($multiple)
            multiple
            @endisset
            @isset($tags)
            data-tags="true"
            @endisset
    >
        @foreach ($options as $option)
            @if (isset($option->id))
                <option value="{{ $option->id ?? ''}}"
                        @isset($selected)
                            @if((is_array($selected)&& in_array($option->id, $selected))||$option->id===$selected)
                            selected
                            @endif
                        @endisset
                        @isset($showUrl)
                        data-show-consent-url="{{route(config('laraconsent.routes.admin.prefix').'.show',['consentOption'=>$option->id])}}"
                        @endisset
                >
                    {{$option->name??''}}
                </option>
            @endif
        @endforeach
    </select>

</div>