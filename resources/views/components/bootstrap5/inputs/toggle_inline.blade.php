<div class="{{$wrapperClass??"mb-4"}}">
    <div class="form-check form-switch  @error($name) is-invalid @enderror">
        <input class="form-check-input {{$class??''}}" type="checkbox" value="{{$value??0}}" id="{{$name}}" name="{{$name}}"
                {{old($name)=="1"?'checked ':''}}
                @if(isset($checked) && $checked)
                checked
                @endisset
                @if((isset($readonly) && $readonly) || isset($unavailable) && $unavailable)
               readonly
               disabled
                @endisset
        >
        <label class="form-check-label" for="{{$name}}">
            {{$label??''}}
            @isset($required)
                <span class="text-danger">*</span>
            @endif
        </label>
    </div>
</div>

