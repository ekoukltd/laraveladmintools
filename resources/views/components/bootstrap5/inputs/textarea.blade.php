<div class="mb-4">

    <label class="form-label d-flex align-items-center">{{ $label ?? '' }}
        @isset($required)
            &nbsp;<span class="text-danger">*</span>
        @endisset
        @isset($btnAfter)
        <button type="button" class="btn {{$btnAfterClass ?? ' ms-auto btn-info'}}">{{$btnAfter}}</button>
        @endisset
    </label>
    <textarea
            name="{{ $name ?? '' }}"
            id="{{ $id ?? '' }}"
            class="input form-control {{ $class ?? '' }}"
            rows="{{ $rows ?? 10 }}"
            cols="{{ $cols ?? 50}}"
            placeholder="{{ $placeholder ?? '' }}"
            @isset($required)
                required
            @endisset
>@isset($value){{ $value }}@endisset</textarea>
</div>
