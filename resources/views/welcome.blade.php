@extends('lat::layouts.backend')
@section('metaTitle','Admin Tools Dashboard')
@section('pageHeader')
    <x-admintools-page-title icon="toolbox" title="Admin Tools" subtitle="Pro Developer Utilities" />
@endsection


@section('content')

    <!-- Page Content -->
        <div class="row items-push">
            @includeIf('ekoukltd::menu.dashboard')
            @includeIf('laraconsent::menu.dashboard')
        </div>
    <!-- END Page Content -->
@endsection
