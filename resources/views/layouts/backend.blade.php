<!doctype html>
<html lang="{{ config('app.locale') }}">

<head>
    @include('lat::layouts.partials.head')
</head>

<body>

<div id="page-container" class="sidebar-o enable-page-overlay sidebar-dark page-header-dark dark-mode side-scroll page-header-fixed main-content-narrow">

@include("lat::layouts.partials.right_side_bar")
@include('lat::layouts.partials.left_side_bar')
@include('lat::layouts.partials.header')


    <!-- Main Container -->
    <main id="main-container">
        @include('lat::layouts.partials.hero')

        <div class="content">

            @include('lat::layouts.partials.notification_banner')

            @yield('content')
        </div>
    </main>
    <!-- END Main Container -->

    @include('lat::layouts.partials.footer')

</div>
<!-- END Page Container -->
@include('vendor.ekoukltd.laravel-admin-tools.components.modals.empty')

<!-- OneUI Core JS -->
<script src="{{ mix('js/oneui.app.js') }}"></script>

<!-- Laravel Scaffolding JS -->
<script src="{{ mix('/js/app.js') }}"></script>

@stack('scripts')
@yield('js_after')
</body>

</html>
