<!-- Sidebar Scrolling -->
<div class="js-sidebar-scroll">
    <!-- Side Navigation -->
    <div class="content-side">
        <ul class="nav-main">
            <li class="nav-main-item">
                <a class="nav-main-link{{ request()->is('dashboard') ? ' active' : '' }}" href="{{route(config('laravel-admin-tools.routes.admin.prefix').'.dashboard')}}">
                    <i class="nav-main-link-icon si si-cursor"></i>
                    <span class="nav-main-link-name">Dashboard</span>
                </a>
            </li>
            @includeIf('ekoukltd::menu.submenu')
            @includeIf('laraconsent::menu.submenu')
        </ul>
    </div>
    <!-- END Side Navigation -->
</div>
<!-- END Sidebar Scrolling -->