<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">

<title>@yield('metaTitle') |  {{config('app.name')}}</title>

<meta name="description" content="Laravel Admin Tools">
<meta name="author" content="Eko UK Ltd">
<meta name="robots" content="noindex, nofollow">

<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">

<!-- Icons -->
<link rel="shortcut icon" href="{{ asset('media/favicons/favicon.png') }}">
<link rel="icon" sizes="192x192" type="image/png" href="{{ asset('media/favicons/favicon-192x192.png') }}">
<link rel="apple-touch-icon" sizes="180x180" href="{{ asset('media/favicons/apple-touch-icon-180x180.png') }}">

<!-- Fonts and Styles -->
@yield('css_before')
<link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700&display=swap">

<link rel="stylesheet" href="{{ asset('css/plugins.css') }}">
<link rel="stylesheet" id="css-main" href="{{ mix('/css/oneui.css') }}">

<!-- You can include a specific file from public/css/themes/ folder to alter the default color theme of the template. eg: -->
<!-- <link rel="stylesheet" id="css-theme" href="{{ mix('/css/themes/amethyst.css') }}"> -->
@yield('css_after')

<!-- Scripts -->
<script>
    window.Laravel = {!! json_encode(['csrfToken' => csrf_token()]) !!};
</script>