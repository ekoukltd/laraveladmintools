@if ($msg = Session::get('error'))
    @push('scripts')
        <script>
            jQuery(function () {
                UserNotificationPrefs.helpers('notify', {type: 'danger', icon: 'fa fa-alert mr-1', message: '{!! $msg  !!}'});
            });
        </script>
    @endpush
    <x-admintools-alert type="danger" :message="$msg"/>
@endif


@if ($msg = Session::get('success'))
    @push('scripts')
        <script>
            jQuery(function () {
                UserNotificationPrefs.helpers('notify', {type: 'success', icon: 'fa fa-check-circle mr-1', message: '{!! $msg  !!}'});
            });
        </script>
    @endpush
    <x-admintools-alert type="success" :message="$msg"/>
@endif

@if ($msg = Session::get('status'))
    @push('scripts')
        <script>
            jQuery(function () {
                UserNotificationPrefs.helpers('notify', {type: 'success', icon: 'fa fa-check-circle mr-1', message: '{!! $msg  !!}'});
            });
        </script>
    @endpush
    <x-admintools-alert type="success" :message="$msg"/>
@endif

@if ($msg = Session::get('message'))
    @push('scripts')
        <script>
            jQuery(function () {
                UserNotificationPrefs.helpers('notify', {type: 'success', icon: 'fa fa-info-circle mr-1', message: '{!! $msg  !!}'});
            });
        </script>
    @endpush
    <x-admintools-alert type="success" :message="$msg"/>
@endif

@if ($msg = Session::get('success-negative'))
    @push('scripts')
        <script>
            jQuery(function () {
                UserNotificationPrefs.helpers('notify', {type: 'warning', icon: 'fa fa-info-circle mr-1', message: '{!! $msg  !!}'});
            });
        </script>
    @endpush
    <x-admintools-alert type="success" :message="$msg"/>
@endif
