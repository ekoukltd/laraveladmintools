# LaravelAdminTools

[![Latest Version on Packagist][ico-version]][link-packagist]
[![Total Downloads][ico-downloads]][link-downloads]
[![Build Status][ico-travis]][link-travis]
[![StyleCI][ico-styleci]][link-styleci]

This is where your description should go. Take a look at [contributing.md](contributing.md) to see a to do list.

## Installation

Via Composer

``` bash
$ composer require ekoukltd/laravel-admin-tools
```

For Developers who want to symlink resources to vendor package
```bash
mkdir -p resources/assets/ekoukltd
cd resources/assets/ekoukltd
ln -s ../../../packages/ekoukltd/laravel-admin-tools/resources/assets  laravel-admin-tools
ln -s ../../../packages/ekoukltd/laraconsent/resources/assets  laraconsent
ln -s ../../../packages/ekoukltd/user-notification-prefs/resources/assets  user-notification-prefs
```

To Symlink views directory, change back to home directory then:-
```bash
mkdir -p resources/views/vendor/ekoukltd
cd resources/views/vendor/ekoukltd
ln -s ../../../packages/ekoukltd/laravel-admin-tools/resources/views  laravel-admin-tools
ln -s ../../../packages/ekoukltd/laraconsent/resources/views  laraconsent
ln -s ../../../packages/ekoukltd/user-notification-prefs/resources/views  user-notification-prefs
```

Add JS Dependencies in package.json
```javascript

 "dependencies": {
        "@popperjs/core": "^2.10.1",
        "bootstrap": "^5.1.1",
         "bootstrap-notify": "^3.1.3",
         "datatables.net-bs5": "^1.11.4",
         "datatables.net-buttons-bs5": "^2.2.2",
         "datatables.net-responsive": "^2.2.9",
         "datatables.net-responsive-bs5": "^2.2.9",
         "datatables.net-scroller-bs5": "^2.0.5",
         "flatpickr": "^4.6.9",
         "froala-editor": "^4.0.9",
         "jquery": "^3.6.0",
         "jszip": "^3.7.1",
         "moment": "^2.29.1",
         "pdfmake": "^0.1.72",
         "select2": "^4.1.0-rc.0",
         "simplebar": "^5.3.5",
         "summernote": "0.8.18",
         "tinykeys": "^1.3.0"
    }
```

Import javascript files into resources/js/app.js
```javascript
import LaraAdminTools from "../assets/ekoukltd/laravel-admin-tools/js/lara-admin-tools";
import Laraconsent from "../assets/ekoukltd/laraconsent/js/laraconsent";
import UserNotificationPrefs from "../assets/ekoukltd/user-notification-prefs/js/user-notification-prefs";
```

## Usage

## Change log

Please see the [changelog](changelog.md) for more information on what has changed recently.

## Testing

``` bash
$ composer test
```

## Contributing

Please see [contributing.md](contributing.md) for details and a todolist.

## Security

If you discover any security related issues, please email author@email.com instead of using the issue tracker.

## Credits

- [Author Name][link-author]
- [All Contributors][link-contributors]

## License

MIT. Please see the [license file](license.md) for more information.

[ico-version]: https://img.shields.io/packagist/v/ekoukltd/laravel-admin-tools.svg?style=flat-square
[ico-downloads]: https://img.shields.io/packagist/dt/ekoukltd/laravel-admin-tools.svg?style=flat-square
[ico-travis]: https://img.shields.io/travis/ekoukltd/laravel-admin-tools/master.svg?style=flat-square
[ico-styleci]: https://styleci.io/repos/12345678/shield

[link-packagist]: https://packagist.org/packages/ekoukltd/laravel-admin-tools
[link-downloads]: https://packagist.org/packages/ekoukltd/laravel-admin-tools
[link-travis]: https://travis-ci.org/ekoukltd/laravel-admin-tools
[link-styleci]: https://styleci.io/repos/12345678
[link-author]: https://github.com/ekoukltd
[link-contributors]: ../../contributors
