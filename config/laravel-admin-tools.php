<?php

return [
    'routes'     => [
        'admin' => [
            //Admin web routes should only be available to admins
            'prefix'     => 'admin-tools',
            'middleware' => ['web','auth']
        ],
    ],

    //Which templates to use: bootstrap4, bootstrap5, tailwind
    //This will use the appropriate view paths
    'css_format' =>'bootstrap5',

    'datatables'=>[
        'dom'=>[
            'bootstrap4'=>"<'row'<'col-sm-12 text-right'B>><'row'<'col-sm-12 col-md-6 text-left'f><'col-sm-12 col-md-6 text-right'i>><'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5 mb-3'l><'col-sm-12 col-md-7 mb-3'p>>",
            'bootstrap5'=>"<'row'<'col-sm-12 text-end'B>><'row'<'col-sm-12 col-md-6 text-start'f><'col-sm-12 col-md-6 text-end'i>><'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5 mb-3'l><'col-sm-12 col-md-7 mb-3'p>>",
            'tailwind'=>""
        ],
        'processing'=>'<span class="text-warning"><i class="fa fa-2x fa-cog fa-spin me-3 mr-3" style=" vertical-align: middle;"></i> Loading Data</span>'

    ]
];