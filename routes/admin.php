<?php

use Illuminate\Support\Facades\Route;
use Ekoukltd\LaravelAdminTools\Http\Controllers\AdminController;

Route::group([
    'middleware' => config('laravel-admin-tools.routes.admin.middleware'),
    'prefix'=>config('laravel-admin-tools.routes.admin.prefix')
             ], function() {
    //Routes for admin tools
    Route::get('/', [AdminController::class,'dashboard'])->name(config('laravel-admin-tools.routes.admin.prefix').'.dashboard');
});


